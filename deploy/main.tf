terraform {
  backend "s3" {
    bucket         = "mogul-api-devops-tfstate"
    key            = "mogul-api.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "mogul-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-west-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
